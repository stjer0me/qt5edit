#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQmlEngine>
#include "textprocessor.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    TextProcessor textProcessor;
    qmlRegisterType<TextProcessor>("com.TextProcessor", 1, 0, "TextProcessor");
    engine.rootContext()->setContextProperty("textProcessor", textProcessor.literalText());
    engine.load(":/main.qml");
    if (engine.rootObjects().isEmpty())
      return -1;



    return app.exec();
}
