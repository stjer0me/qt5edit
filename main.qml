import QtQuick 2.11
import QtQuick.Window 2.11
import QtQuick.Controls 1.4
import com.TextProcessor 1.0

Window {
    id: root
    visible: true
    width: 1200
    height: 800
    title: qsTr("QML MD")

    Rectangle {

        id: editorArea
        focus: true
        width: root.width - 100
        height: root.height - 25
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom

        Text {

            text: textProcessor.textProperty
        }


    }

}
