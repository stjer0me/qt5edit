#ifndef TEXTPROCESSOR_H
#define TEXTPROCESSOR_H

#include <QObject>
#include <QString>

class TextProcessor : public QObject
{

    Q_OBJECT
    Q_PROPERTY(QString literalText READ literalText WRITE writeLiteralText NOTIFY literalTextChanged)

private:
    QString temp = "text read";

public:
    TextProcessor();

    QString textProperty = "text property";

    QString writeLiteralText(QString &str) {

        QString ootput = "text from the function";
        emit literalTextChanged();
        return ootput;

    }

    QString literalText() const {

        return "literalText const";

    }

signals:

    void literalTextChanged();

};

#endif // TEXTPROCESSOR_H
